package Shape;
/**
 * 
 * @author Tyler Martinez CST135
 *
 */
import base.ShapeBase;

public class Triangle extends ShapeBase {
	public Triangle(String name, int width, int height)
	{
		super(name,width,height);
	}
	/**
	 * @return will return the area of a triangle given its height and width
	 */
	public double calculateArea()
	{
		return (width*height)/2;
	}
}
