package Shape;
/**
 * 
 * @author Tyler Martinez CST135
 *
 */
import base.ShapeBase;

public class Trapezoid extends ShapeBase {
	
	double width2=0;
	/**
	 * 
	 * {@inheritDoc}
	 * @param width2 the second width is the other base of the trapezoid that is needed to calculate the area
	 */
	public Trapezoid(String name, int width, int height,double width2)
	{
		super(name,width,height);
		this.width2 = width2;
	}
	/**
	 * @return will return the area of a trapezoid given its height and both widths
	 */
	public double calculateArea()
	{
		return ((width +width2)/2) * height;
	}
}
