package Shape;
/**
 * 
 * @author Tyler Martinez CST135
 *
 */
import base.ShapeBase;

public class Rectangle extends ShapeBase {
	
	public Rectangle(String name, int width, int height)
	{
		super(name,width,height);
	}
	/**
	 * @return will return the area of a rectangle given its height and width
	 */
	public double calculateArea()
	{
		return width*height;
	}
}
