package Shape;
/**
 * 
 * @author Tyler Martinez CST135
 *
 */
import base.ShapeBase;

public class Circle extends ShapeBase {
	public Circle(String name, int width, int height)
	{
		super(name,width,height);
	}
	/**
	 * @return will return the area of a circle given its width
	 */
	public double calculateArea()
	{
		return (Math.PI) *(width);
	}
}
