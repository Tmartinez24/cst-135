package base;
/**
 * 
 * @author Tyler Martinez CST135
 *
 */
interface ShapeInterface {
	/**
	 * 
	 * @return will return the area of a given shape
	 */
	double calculateArea();
}
