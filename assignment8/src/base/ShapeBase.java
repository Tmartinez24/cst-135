package base;
/**
 * 
 * @author Tyler Martinez CST135
 *
 *
 */
public class ShapeBase implements ShapeInterface {
	
	protected String name;
	protected int width;
	protected int height;
	/**
	 * 
	 * @param name the name of the shape
	 * @param width the width of a given shape
	 * @param height the height of a given shape
	 */
	public ShapeBase(String name, int width, int height)
	{
		this.name=name;
		this.width=width;
		this.height=height;
	}
	/**
	 * 
	 * @return will return the name of the shape
	 */
	public String getName()
	{
		return this.name;
	}

	/**
	 * 
	 * @return will return a -1. This usually is not called.
	 */
	@Override
	public double calculateArea() 
	{
		// TODO Auto-generated method stub
		return -1;
	}

}
