package test;
/**
 * 
 * @author Tyler Martinez CST135
 *
 */
import Shape.Circle;
import Shape.Rectangle;
import Shape.Trapezoid;
import Shape.Triangle;
import base.ShapeBase;

class Test {
	/**
	 * 
	 * @param shape the class that contains the data of a shape. The data includes a name a width, and a height
	 */
	private static void displayArea(ShapeBase shape) 
	{
		//show polymorphic by getting shapes name and area
		System.out.println("This is a shape named "+ shape.getName() + " with an area of " + shape.calculateArea());
	}
/**
 * 
 * @param args main method that creates shapes and then calculates their areas.
 */
	public static void main(String[] args) 
	{
		ShapeBase[] shapes= new ShapeBase[4];
		shapes[0] = new Rectangle("Rectangle",10,200);
		shapes[1] = new Triangle("Triangle",10,50);
		shapes[2] = new Circle("Circle",10,100);
		shapes[3] = new Trapezoid("Trapezoid",10,100,35);
		
		for(int x=0;x<shapes.length;++x)
		{
			displayArea(shapes[x]);
		}
	}
}
