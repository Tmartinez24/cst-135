package assignment3;
/**
 * 
 * @author Tyler Martinez CST135
 *
 */
public class Engine extends Car {
	
	private boolean Start;
	/**
	 * The class Engine is a child of car. The car has 1 engine.
	 * @see Car.java for mph and mode
	 * @param mph
	 * @param mode
	 * @param start shows the state of the engine. Shows if it is on or off.
	 */
	public Engine(int mph, String mode, boolean start)
	{
		super.setMode(mode);
		super.setMph(mph);
		this.Start = start;
	}
	/**
	 * sets the start state for the engine
	 * @param start
	 */
	public void setStart(boolean start)
	{
		this.Start = start;
	}
	/**
	 * gets the start state for the engine
	 * @return
	 */
	public boolean getStart()
	{
		return Start;
	}

}
