package assignment3;
/**
 * 
 * @author Tyler Martinez CST135
 *
 */
public class Car {

	private int Mph;
	private String Mode;
	/**
	 * The class car is the parent of tire and engine. The class has 4 wheels and 1 engine.
	 */
	public Car()
	{
		
	}
	/**
	 * creates the class Car
	 * @param mph the miles per hour of the car
	 * @param mode the state that the car is in
	 */
	public Car(int mph, String mode)
	{
		super();
		this.Mph = mph;
		this.Mode= mode;
	}
	/**
	 * set the mph for the car
	 * @param mph
	 */
	public void setMph(int mph)
	{
		this.Mph = mph;
	}
	/**
	 * gets the mph for the car
	 * @return
	 */
	public int getMph()
	{
		return Mph;
	}
	/**
	 * sets the mode of the car. (Stopped or running)
	 * @param mode
	 */
	public void setMode(String mode)
	{
		this.Mode = mode;
	}
	/**
	 * gets the mode of the car. (Stopped or running)
	 * @return
	 */
	public String getMode()
	{
		return Mode;
	}

}
