package assignment3;
/**
 * 
 * @author Tyler Martinez CST135
 *
 */
public class Tire extends Car{
	
	private int Psi1;
	private int Psi2;
	private int Psi3;
	private int Psi4;
	/**
	 * The class Tire is a child of car. The car has 4 tires.
	 * @see Car.java for mph and mode
	 * @param mph
	 * @param mode
	 * @param psi1 the psi for the first tire
	 * @param psi2 the psi for the second tire
	 * @param psi3 the psi for the third tire
	 * @param psi4 the psi for the fourth tire
	 */
	public Tire(int mph, String mode, int psi1, int psi2, int psi3, int psi4)
	{
		super.setMph(mph);
		super.setMode(mode);
		this.Psi1 = psi1;
		this.Psi2 = psi2;
		this.Psi3 = psi3;
		this.Psi4 = psi4;
	}
	/**
	 * sets the psi for the first tire.
	 * @param psi1
	 */
	public void setPsi1(int psi1)
	{
		this.Psi1 = psi1;
	}
	/**
	 * gets the psi for the first tire.
	 * @return
	 */
	public int getPsi1()
	{
		return Psi1;
	}
	/**
	 * sets the psi for the second tire.
	 * @param psi2
	 */
	public void setPsi2(int psi2)
	{
		this.Psi2 = psi2;
	}
	/**
	 * gets the psi for the second tire.
	 * @return
	 */
	public int getPsi2()
	{
		return Psi2;
	}
	/**
	 * sets the psi for the third tire.
	 * @param psi3
	 */
	public void setPsi3(int psi3)
	{
		this.Psi3 = psi3;
	}
	/**
	 * gets the psi for the third tire.
	 * @return
	 */
	public int getPsi3()
	{
		return Psi3;
	}
	/**
	 * sets the psi for the fourth tire.
	 * @param psi4
	 */
	public void setPsi4(int psi4)
	{
		this.Psi4 = psi4;
	}
	/**
	 * gets the psi for the fourth tire.
	 * @return
	 */
	public int getPsi4()
	{
		return Psi4;
	}

}
