package assignment3;
import java.util.Scanner;
/**
 * 
 * @author Tyler Martinez CST135
 *
 */
public class Driver {

	static Car car = new Car(0,"Stopped");
	static Tire tires = new Tire(0, "Stopped", -1, -1, -1, -1);
	static Engine engine = new Engine(0,"Stopped",false);
	/**
	 * The main method of the executable file driver. 
	 * The main method contains the method startCar() which will then lead into a switch statement for starting and runnign the car.
	 * @param args
	 */
	public static void main(String[] args) {
		startCar();

		
	}
	/**
	 * startCar() will prompt the user to either start the car or exit it since it is turned off.
	 * You can only exit the car if it is not running or moving.
	 * Case s/S: will put the user into setting up their tire's psi if it has not already been set up.
	 * Case e/E: will exit the car and thus exit the program
	 */
	public static void startCar()
	{
		System.out.println("Your car is "+ car.getMode() +  " currently. Do you want to start, or exit it?\n(S/E)");
		Scanner scan = new Scanner(System.in);
		String input = scan.nextLine();
		//System.out.println(input);
		
		switch(input)
		{
		case "s": case "S":
			if(tires.getPsi1()!= -1 ||tires.getPsi2()!= -1 || tires.getPsi3()!= -1|| tires.getPsi4()!= -1)
			{
				runningCar();
				engine.setStart(true);
				car.setMode("Running");
			}
			else 
			{
			System.out.println("What is the Psi in your tires?");
			enterPsi();
			}
			break;
			
		case "e": case "E":
			exitCar();
			break;
			
		default:
			break;
		}
		scan.close();
	}
	/**
	 * With the car running you can choose to drive the car and change the mph/speed or turn it off.
	 * Case 1: Will send the user to the driveCar() method where they can choose to accelerate or decelerate.
	 * Case 2: Will send the user to turnoffCar() where they will shut off the car.
	 */
	public static void runningCar()
	{
		System.out.println("Your car is "+ car.getMode() + " currently. Do you want to:\n{1}Drive\n{2}Turn off");
		Scanner scan = new Scanner(System.in);
		String input = scan.nextLine();
		//System.out.println(input);
		
		switch(input)
		{
		case "1":
			driveCar();
			break;
			
		case "2":
			turnoffCar();
			break;
			
		default:
			break;
		}
		scan.close();
	}
	/**
	 * The user will be prompted with changing the speed of the car of stopping it.
	 * Case 1: The user will be prompted to enter a speed from 1 to 60
	 * Case 2: The user can stop the car, but only if it is at the lowest mph it can go
	 */

	public static void driveCar() {
		System.out.println("Your car is going "+ car.getMph() + " mph. Do you want to:\n{1}Change Speed\n{2}Stop");
		Scanner scan = new Scanner(System.in);
		String input = scan.nextLine();
		
		switch(input)
		{
		case "1":
			System.out.println("How fast do you want to go? Please choose a number from 1 to 60.\n");
			int speed = scan.nextInt();
			car.setMph(speed);
			driveCar();
			break;

		case "2":
			if((car.getMph()!= 1))
			{
				System.out.println("Please reduce speed to 1mph before stopping.\n");
				driveCar();
			}
			else
			{
				runningCar();
			}
			break;
			
		default:
			break;
		}
		scan.close();
	
	
	}
	/**
	 * This will turn the car off and then send the user back startCar() where the user may choose to restart the car or to exit the car.
	 */
	public static void turnoffCar() {
		engine.setStart(false);
		startCar();
	}
		
		

	/**
	 * The user will be prompted to enter the psi of each of his tires. 
	 * If any of the tires are below 32psi then the car will not start until the user fills his tires.
	 */
	public static void enterPsi()
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Psi for Tire 1?");
		int tire1 = scan.nextInt();
		System.out.println("Psi for Tire 2?");
		int tire2 = scan.nextInt();
		System.out.println("Psi for Tire 3?");
		int tire3 = scan.nextInt();
		System.out.println("Psi for Tire 4?");
		int tire4 = scan.nextInt();
		
		tires.setPsi1(tire1);
		tires.setPsi2(tire2);
		tires.setPsi3(tire3);
		tires.setPsi4(tire4);
		
		if((tires.getPsi1()) < 32 ||(tires.getPsi2()) < 32 || (tires.getPsi3()) < 32|| (tires.getPsi4()) < 32)
		{
			System.out.println("Your tires Psi is too low. They need to be a minimum of 32 Psi.\n");
			startCar();
			scan.close();
		}
		else 
		{
			//System.out.println("Your car is now running.");
			engine.setStart(true);
			tires.setMode("Running");
			car.setMph(1);
			runningCar();
			scan.close();
		}
	}
	/**
	 * exitCar() will exit both the car and the program
	 */
	public static void exitCar()
	{
		System.out.println("Exiting");
		System.exit(0);
	}
}
