/** @author Tyler Martinez
 *  @author CST135
 *  
 *  @author The class Person contains getters and setters for the class person which includes the persons age, name, and weight. 
 *  @author When the program is ran it will display the persons name and then run two methods. 
 *  @author Those methods will then display their own message showing that they were successfully entered.
 */

package assignment2;

public class Person { 

	private int age;
	private String name;
	private float weight;
	/**
	 * 
	 * @param age
	 * @param name
	 * @param weight
	 */
	
	public Person(int age, String name, float weight) {
		
		super();
		this.age=age;
		this.name=name;
		this.weight=weight;
	}
	/**
	 * 
	 * @return age
	 */
	public int getAge()
	{
		return age;
	}
	/**
	 * 
	 * @return name
	 */
	public String getName()
	{
		return name;
	}
	/**
	 * 
	 * @return weight
	 */
	public float getWeight()
	{
		return weight;
	}
	/**
	 * 
	 * @param age
	 */
	public void setAge(int age) {
		this.age = age;
	}
	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 
	 * @param weight
	 */
	public void setWeight(float weight) {
		this.weight = weight;
	}
	
	public void walk()
	{
		System.out.println("I am in walk()");
	}
	/**
	 * 
	 * @param distance
	 * @return 0
	 */
	public float run(float distance)
	{
		System.out.println("I am in run()");
		return 0;
	}
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Person person = new Person(26, "Tyler" , (float)203.30);
		System.out.println("My name is " + person.getName());
		person.walk();
		person.run(10);

	}

}
